#!/usr/bin/env python

import argparse
import cairo
import collections
import gdal
import math
import numpy
import os
import sys

from collections import defaultdict
from math import cos,pi
from scipy.ndimage import zoom

import pickle as pickle

from SampleConfig import SampleConfig

PIXELS_PER_INCH = PPI = 72

def shrink(data, rows, cols):
    print("shape:", data.shape)
    print("rows:", rows)
    print("cols:", cols)
    return data.reshape(rows, data.shape[0]/rows, cols, data.shape[1]/cols).max(axis=1).max(axis=2)

def create_page_row(prefix, row, pss, crs, pages_wide, page_width, page_height, format):
    print("create_page_row:", row)
    pss.append([])
    crs.append([])
    if format == 'svg':
        Surface = cairo.SVGSurface
    elif format == 'pdf':
        Surface = cairo.PDFSurface
    for i in range(pages_wide):
        pss[row].append(Surface("%s_%02d_%02d.%s" % (prefix,row,i,format), page_width, page_height))
        cr = cairo.Context(pss[row][-1])
        cr.set_source_rgb(0,0,0)
        cr.set_line_width(0.1)
        cr.move_to(0,0)
        crs[row].append(cr)

class Drawer:
    def __init__(self, config, state=None):
        self.data_stat = None
        self.config = config
        self.old_config = None
        self.state = None

        self.dependencies = {
            'data': set((
                'rows',
                'scale_y',
                'window_function',
                'window_width'
            )),
            'contour': set((
                'output_width',
                'output_height',
                'output_resolution',
                'page_width',
                'page_height',
                'min_slice',
                'node_depth',
                'rib_spacing',
                'rib_width',
                'rib_height',
                'profile',
                'label_font',
                'cut_color',
                'interior_color',
                'etch_color',
                'kerf',
                'format'
            )),
        }

    def set_state(self, state):
        self.state = state
        self.old_config = None

    def save_state(self):
        if self.state is not None:
            f = open(self.state, "w")
            pickle.dump(self, f)
            f.close()

    def update_config(self, config):
        self.old_config = self.config
        self.config = config

    def check_dependencies(self, category):
        if category not in self.dependencies:
            return True
        if self.config.force_reload:
            return True
        if self.old_config is not None:
            for k in self.dependencies[category]:
                if k == 'window_width':
                    import pdb;pdb.set_trace()
                if self.config[category][k] != self.old_config[category][k]:
                    print("NON_MATCHING CONFIG: new[%s] = %s, old[%s] = %s" % (k, self.config[category][k], k, self.old_config[category][k]))
                    return True
        return False

    def smooth(self, x,window_len=11,window='hanning'):
        """smooth the data using a window with requested size.

        This method is based on the convolution of a scaled window with the signal.
        The signal is prepared by introducing reflected copies of the signal
        (with the window size) in both ends so that transient parts are minimized
        in the begining and end part of the output signal.

        input:
            x: the input signal
            window_len: the dimension of the smoothing window; should be an odd integer
            window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
                flat window will produce a moving average smoothing.

        output:
            the smoothed signal

        example:

        t=linspace(-2,2,0.1)
        x=sin(t)+randn(len(t))*0.1
        y=smooth(x)

        see also:

        numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
        scipy.signal.lfilter

        TODO: the window parameter could be the window itself if an array instead of a string
        NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
        """

        if x.ndim != 1:
            raise ValueError("smooth only accepts 1 dimension arrays.")

        if x.size < window_len:
            raise ValueError("Input vector needs to be bigger than window size.")


        if window_len<3:
            return x


        if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
            raise ValueError("Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


        s=numpy.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
        #print(len(s))
        if window == 'flat': #moving average
            w=numpy.ones(window_len,'d')
        else:
            w=eval('numpy.'+window+'(window_len)')

        y=numpy.convolve(w/w.sum(),s,mode='valid')
        return y

    def smooth_all(self):
        #s = numpy.empty(shape=(d.shape[0],d.shape[1]+window-1))
        window = self.config.window_width
        for i in range(len(self.data)):
            self.data[i] = self.smooth(self.data[i],window=self.config.window_function,window_len=window)[int(window/2):-int(window/2)]

    def read_array(self, fn):
        a1 = gdal.Open(fn)
        a2 = a1.GetRasterBand(1)
        a3 = a2.ReadAsArray()
        return a3

    def need_data(self):
        if self.data_stat is None:
            print("NO DATA STAT")
            return True
        for row in self.config['data'].rows:
            for tile in row.split(","):
                if tile not in self.data_stat or self.data_stat[tile] != os.stat(tile):
                    print("NEEDS RELOAD:", tile)
                    return True
        print("DON'T NEED DATA")
        return False

    def get_data(self):
        rows = []
        self.data_stat = {}
        for row in self.config['data'].rows:
            tiles = []
            for tile in row.split(","):
                tiles.append(self.read_array(tile))
                self.data_stat[tile] = os.stat(tile)
            res = tiles[0]
            for tile in tiles[1:]:
                res = numpy.concatenate((res,tile),1)
            rows.append(res)
        d = rows[0]
        for row in rows[1:]:
            d = numpy.concatenate((d,row),0)

        return d

    def prepare_data(self):
        if not self.check_dependencies('data') and not self.need_data():
            print("DON'T NEED DATA")
            return

        print("NEEDED DATA")

        d = self.get_data()

        if self.config.rotate:
            d = numpy.swapaxes(d,0,1)

        print("Output dimensions (inches): %sx%s" % (self.config.output_height/PPI, self.config.output_width/PPI))
        print("Output dimensions (pixels): %sx%s" % (self.config.output_height, self.config.output_width))

        print("Raw data dimensions: %sx%s" % d.shape)

        # Scale elevation data to x/y resolution
        meters_per_degree_lat = 111132.92 - 559.82 * cos(2* float(self.config.latitude)*3.14159/180)+1.175*cos(4*0.872665)
        meters_per_sample = meters_per_degree_lat/(3600.0/float(self.config.resolution))
    
        print("meters_per_degree_lat:", meters_per_degree_lat)
        print("meters_per_sample:", meters_per_sample)

        d = (d/meters_per_sample)

        # y_range is number of slices to make the full design -- height/thickness
        y_range = int(float(self.config.output_height)/float(self.config.thickness))

        # Adjust actual output size to match integer number of slices
        new_height = y_range*self.config.thickness/PPI
        self.config.output_width = (self.config.output_width/self.config.output_height)*new_height
        self.config.output_height = new_height

        # x_range is number of points across a slice
        x_range = int(self.config.output_width*self.config.output_resolution)

        print("y_range:", y_range)
        print("x_range:", x_range)

        print("adjusted output dimensions: %5.2fx%5.2f" % (self.config.output_height/PPI, self.config.output_width/PPI))

        # crop data in order to maintain aspect ratio
        if self.config.output_width/self.config.output_height < float(d.shape[1])/float(d.shape[0]):
            # Source data is too wide -- crop width
            y_crop = 0.0
            x_crop = float(d.shape[1])-(self.config.output_width/self.config.output_height)*float(d.shape[0])
        else:
            # Source data is too tall -- crop height
            x_crop = 0.0
            y_crop = float(d.shape[0])-(self.config.output_height/self.config.output_width)*float(d.shape[1])

        print("y_crop:", y_crop)
        print("x_crop:", x_crop)

        y_crop_pct = float(y_crop)/float(d.shape[0])
        x_crop_pct = float(x_crop)/float(d.shape[1])
        print("y_crop_pct:", y_crop_pct)
        print("x_crop_pct:", x_crop_pct)

        if x_crop > 0.0 and y_crop > 0.0:
            d = d[int(y_crop/2):int(-y_crop/2),int(x_crop/2):int(-x_crop/2)]
        elif x_crop > 0.0:
            d = d[:,int(x_crop/2):int(-x_crop/2)]
        elif y_crop > 0.0:
            d = d[int(y_crop/2):int(-y_crop/2),:]

        print("Cropped data dimensions: %sx%s" % d.shape)

        if abs(y_crop_pct) > 0.05:
            print("WARNING: Cropped %4.2f%% of data in y dimension -- consider manually cropping input data to match output aspect ratio" % (y_crop_pct*100.0))
        if abs(x_crop_pct) > 0.05:
            print("WARNING: Cropped %4.2f%% of data in x dimension -- consider manually cropping input data to match output aspect ratio" % (x_crop_pct*100.0))

        # Resample data to fit better into output dimensions
        zoom_factor = (float(y_range)/float(d.shape[0]),float(x_range)/float(d.shape[1]))
        print("zoom_factor:", zoom_factor)
        #if zoom_factor > 1.0:
        #   print "ERROR: bad zoom factor"
        #   sys.exit(1)
        d = zoom(d, zoom_factor)

        print("Zoomed data dimensions: %sx%s" % d.shape)

        # Sometimes zooming will drop a few samples -- resize (zero-fill data) to fix aspect ratio after zoom
        # This will not affect the output since the shrink function will take the max of a range of the data
        y_start = int(round((float(d.shape[0])/float(y_range)))*float(y_range))
        x_start = int(round((float(d.shape[1])/float(x_range)))*float(x_range))

        # Rounding error above can cause problems -- make sure we are really only zero-filling a few samples
        #if abs(y_start-d.shape[0] > 5):
        #   y_start = d.shape[0]
        #if abs(x_start-d.shape[1]) > 5:
        #   x_start = d.shape[1]

        print("y_start:", y_start)
        print("x_start:", x_start)

        d.resize((y_start,x_start))
        print("Resized data dimensions: %sx%s" % d.shape)

        # resize data to desired number of samples
        #d = shrink(d, y_range, x_range)

        # Dropping one X sample because it is sometimes bad -- not sure why
        d = d[:,0:-1]

        d = d[:,]*self.config.scale_y

        print("Final data dimensions: %sx%s" % d.shape)

        print("page_width:", self.config.page_width)
        print("page_height:", self.config.page_height)

        # Rib calculations
        self.config.rib_spacing = int(self.config.output_width/self.config.num_ribs)
    
        print("rib_spacing:", self.config.rib_spacing)
        print("rib_width:", self.config.rib_width)
        print("rib_height:", self.config.rib_height)

        # Crossbar calculations
        self.config.xbar_spacing = int(y_range/self.config.num_xbars)
        print("xbar_spacing:", self.config.xbar_spacing)

        self.data = d

        if self.config.window_function.lower() != 'none':
            self.smooth_all()

        self.save_state()

    def draw(self):
        self.prepare_data()
        self.draw_contour()
        self.draw_ribs()
        self.draw_xbars()

    def draw_contour(self):
        d = self.data 
        width = self.config.output_width
        height = self.config.output_height
        resolution = self.config.output_resolution
        page_width = self.config.page_width
        page_height = self.config.page_height
        min_slice = self.config.min_slice
        node_depth = self.config.node_depth
        rib_spacing = self.config.rib_spacing
        rib_width = self.config.rib_width
        rib_height = self.config.rib_height
        profile = self.config.profile
        label_font = self.config.label_font
        cut_color = self.config.cut_color
        interior_color = self.config.interior_color
        etch_color = self.config.etch_color
        kerf = self.config.kerf
        format = self.config.format

        if width % page_width == 0:
            pages_wide = int(width/page_width)
        else:
            pages_wide = int(width/page_width)+1

        print("pages_wide:", pages_wide)
        print("expected max x:", (pages_wide*page_width))
        print("actual max x:", len(d[0]))
        print("resolution:", resolution)

        pss = []
        crs = []

        y = None
        next_line = None
        next_line_min = None
        create_page_row("c",len(crs), pss, crs, pages_wide, page_width, page_height, format)

        rib_heights = {}
        first_lines = [0]
    
        min_delta_y = None
        min_delta_idx = None
        label_x = None

        rib_width_slices = int(rib_width*resolution)

        for line_idx in range(len(d)):
            print("slice %4d: range %6.2f-%6.2f" % (line_idx, min(d[line_idx][:-1]), max(d[line_idx][:-1])))

            line_min = 0

            # Find next line in order to measure space at each point so slices don't get too thin
            if next_line is not None:
                line_min = next_line_min
                line = next_line
            else:
                # -min() here normalizes y values to 0
                line_min = min(d[line_idx])
                line = d[line_idx]-line_min

            try:
                next_line_min = min(d[line_idx+1])
                next_line = d[line_idx+1]-next_line_min
            except IndexError:
                next_line = None
                next_line_min = None

            #y += max(line)
            if y is None:
                y = max(line)

            x = 0
            delta_y = 0
            cr = None
            mod_crs = {}
            max_rib_y = None
            is_last_line = False
            rib_idx = 0
            label_done = False

            for i in range(len(line)):
                # Temporary -- limit number of pages drawn
                #if y/page_height > 5:
                #   break
                cr = crs[int(y/page_height)][0]
                #except:
                #   data = d
                #   import pdb;pdb.set_trace()

                cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])
                cr.select_font_face(label_font, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
            
                mod_crs[cr] = True
                px = x%page_width
                py = (y-line[i])%page_height
                if i == 0:
                    cr.move_to(px,py)
                else:
                    cr.line_to(px,py)

                # is_rib is true if this point will have a rib cutout below it -- used to ensure minimum slice width
                is_rib = ((x+rib_spacing/2) % rib_spacing) < rib_width
                rib_done = is_rib and rib_done

                # is_label is true if this point will have a numbered label below it to help with assembly
                is_label = not label_done and not is_rib and i > len(line)*.95 and not line_idx in first_lines

                # Draw cutouts for ribs
                if is_rib and not rib_done and not profile:
                    #if i+rib_width_slices > len(line):
                    #   # Too close to edge to make a rib cutout
                    #   continue

                    rib_idx = i / resolution
                    rib_done = True

                    # Draw rib cutout above current line
                    if rib_idx in rib_heights and not line_idx in first_lines:
                        rib_px_left = px+kerf/2.0
                        rib_px_right = px+rib_width-kerf/2.0
                        #rib_top_py = rib_heights[rib_idx][-1]%page_height
                        (last_elevation, last_py) = rib_heights[rib_idx][-1]
                        rib_top_py = (y-rib_height-max(line[int(i):int(i+rib_width)]))%page_height
                        last_delta = rib_top_py-last_py
                        rib_heights[rib_idx][-1] = last_elevation-last_delta
                        rib_end_py = (y-line[i+rib_width_slices])%page_height
                    
                        # Straight line
                        #cr.line_to(px,rib_top_py)
                    
                        # Scaled arc
                        #cr.stroke()
                        #cr.save()
                        #cr.translate(px+2.0/2.0, py+(rib_top_py-py)/2.0)
                        #cr.scale(2.0, (rib_top_py-py)/2.0)
                        #cr.arc(0., 0., 1., 1.5*math.pi, 0.5*math.pi)
                        #cr.stroke()
                        #cr.restore()
                        #cr.move_to(px, rib_top_py)

                        # Bezier curve
                        #curve_height = rib_top_py-py
                        cr.stroke()
                        cr.set_source_rgb(interior_color[0],interior_color[1],interior_color[2])
                        curve_pct = 0.8
                        left_height = py-rib_top_py
                        right_height = rib_end_py-rib_top_py
                        total_curve_height = min(py, rib_end_py)-rib_top_py
                        curve_height = total_curve_height*curve_pct
                        ext_height = total_curve_height*(1.-curve_pct)/2.
                        top_ext_py = rib_top_py+ext_height
                        ext_left = rib_top_py+ext_height+curve_height
                        cr.move_to(rib_px_left,py)
                        cr.line_to(rib_px_left, ext_left)
                        x1=x2 = rib_px_left+node_depth
                        y1 = ext_left-curve_height/3.
                        y2 = ext_left-2.*curve_height/3.
                        cr.curve_to(x1,y1,x2,y2,rib_px_left,top_ext_py)
                        cr.line_to(rib_px_left, rib_top_py)
                        cr.line_to(rib_px_right,rib_top_py)

                        # Straight line
                        #cr.line_to(px+rib_width,rib_end_py)
                    
                        # Bezier curve
                        cr.line_to(rib_px_right,top_ext_py)
                        x1=x2 = rib_px_right-node_depth
                        y2 = top_ext_py+2.*curve_height/3.
                        y1 = top_ext_py+curve_height/3.
                        cr.curve_to(x1,y1,x2,y2,rib_px_right,top_ext_py+curve_height)
                        cr.line_to(rib_px_right,rib_end_py)
                        cr.stroke()
                        cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

                        cr.move_to(px,py)

                    # Determine where to draw top of next rib cutout
                    if rib_idx not in rib_heights:
                        rib_heights[rib_idx] = []
                    # For now, put the actual elevation of the current point into rib_heights
                    # When the rib cutout is drawn above the next line, this will be adjusted to 
                    # be the actual elevation of the top of the rib
                    rib_heights[rib_idx].append(((line[i]+line_min), py))

                    # Draw rib cutout
                    #cr.move_to(px,rib_top_py)
                    #cr.line_to(px+rib_width,rib_top_py)
                    #cr.move_to(px+rib_width,rib_end_y)
                    #cr.move_to(px,py)

                # Draw label for ease of assembly
                if is_label:
                    cur = cr.get_current_point()
                    label_x = cur[0]
                    cr.stroke()
                    cr.move_to(cur[0],cur[1])
                    cr.set_source_rgb(etch_color[0],etch_color[1],etch_color[2])
                    cr.move_to(cur[0],((y-max(line[i:i+40]))%page_height-5))
                    cr.set_font_size(20)
                    cr.show_text(str(line_idx-1))
                    cr.stroke()
                    cr.move_to(cur[0],cur[1])
                    label_done = True
                    cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

                # Determine line offset from next line so that pieces are not cut too thin
                if next_line is not None and not profile:
                    # If current point is above a rib, add extra space
                    if is_rib:
                        rib_space_next = rib_height
                    else:
                        rib_space_next = 0
                    for j in range(max(i-20,0), min(len(line),i+20)):
                        # Ending at len(line)-3 here because sometimes line[len(line)-1]=0, which causes a huge delta -- don't know why
                        if i > len(line)-3:
                            break
                        # FIXME -- this is really slow, probably can be done faster
                        next_idx = min(j,len(line)-1)
                        next_point = next_line[next_idx]
                        cur_point = line[i]
                        #print "delta_y = next_line[min(%s,%s)]-cur_point = %s-%s = %s" % (j,len(line)-1,next_point,cur_point,(next_point-cur_point))
                        delta_y = max(delta_y, next_point+rib_space_next-cur_point)
                x += 1.0/resolution
            #print "delta_y:", delta_y
            #print "y:", y
            if delta_y > 0 and (min_delta_y is None or delta_y < min_delta_y):
                min_delta_y = delta_y
                min_delta_idx = line_idx

            # Determine if this is the last line on the page
            last_line_page = False
            if next_line is None:
                last_line_page = True

            try:
                dummy_cr = crs[int((y+delta_y+min_slice+rib_height)/page_height)][0]
            except IndexError:
                last_line_page = True

            if last_line_page:
                print("last_line_page:", line_idx)
                # Draw end of page stuff for this page
                #start_py = (d[first_lines[-1]][0]-min(d[first_lines[-1]]))%page_height
                #end_py = (d[first_lines[-1]][-1]-min(d[first_lines[-1]]))%page_height
                start_py = 0
                end_py = 0
                bottom_py = (y+min_slice+rib_height)%page_height
                if bottom_py < (y % page_height):
                    bottom_py = page_height

                # Draw left side line on cr[...][0]
                cr = crs[-1][0]
                cr.stroke()
                cr.move_to(0.01,bottom_py)
                cr.line_to(0.01,start_py)
                cr.stroke()

                # Draw right side line on cr[...][-1]
                cr = crs[-1][-1]
                cr.stroke()
                cr.move_to(px,bottom_py)
                cr.line_to(px,end_py)

                # Draw bottom lines
                for i in range(len(crs[-1])):
                    cr = crs[-1][i]
                    cr.stroke()
                    cr.move_to(0.01,bottom_py)
                    cr.line_to(px,bottom_py)
                    cr.stroke()

                # Draw label
                cur = cr.get_current_point()
                cr.stroke()
                cr.move_to(label_x, bottom_py-5)
                cr.set_source_rgb(etch_color[0],etch_color[1],etch_color[2])
                cr.set_font_size(20)
                cr.show_text(str(line_idx))
                cr.stroke()
                cr.move_to(cur[0],cur[1])
                label_done = True
                cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

                # Draw rib cutouts
                x = 0
                rib_done = False
                for i in range(len(line)):
                    is_rib = ((x+rib_spacing/2) % rib_spacing) < rib_width
                    rib_done = is_rib and rib_done
                    px = x % page_width
                    if is_rib and not rib_done and not profile:
                        rib_done = True
                        if i+rib_width_slices > len(line):
                            # Too close to edge to make a rib cutout
                            continue

                        rib_idx = i / resolution

                        # Draw verticals up to previous rib cutout (if any)
                        if rib_idx in rib_heights:
                            #rib_top_py = rib_heights[rib_idx][-1]%page_height
                            rib_top_py = (bottom_py-rib_height-min(line[i:i+rib_width_slices])/2.5)%page_height
                            try:
                                (last_elevation, last_py) = rib_heights[rib_idx][-1]
                            except Exception as exp:
                                import pdb;pdb.set_trace()
                            last_delta = rib_top_py-last_py
                            rib_heights[rib_idx][-1] = last_elevation-last_delta
                            cr.stroke()
                            cr.set_source_rgb(interior_color[0],interior_color[1],interior_color[2])
                            cr.move_to(px,bottom_py)

                            # Straight line
                            #cr.line_to(px,rib_top_py)
                    
                            # Scaled arc
                            #cr.stroke()
                            #cr.save()
                            #cr.translate(px+2.0/2.0, py+(rib_top_py-py)/2.0)
                            #cr.scale(2.0, (rib_top_py-py)/2.0)
                            #cr.arc(0., 0., 1., 1.5*math.pi, 0.5*math.pi)
                            #cr.stroke()
                            #cr.restore()
                            #cr.move_to(px, rib_top_py)

                            # Bezier curve
                            #curve_height = rib_top_py-py
                            curve_pct = 0.8
                            py = bottom_py
                            rib_end_py = bottom_py
                            left_height = py-rib_top_py
                            right_height = rib_end_py-rib_top_py
                            total_curve_height = min(py, rib_end_py)-rib_top_py
                            curve_height = total_curve_height*curve_pct
                            ext_height = total_curve_height*(1.-curve_pct)/2.
                            top_ext_py = rib_top_py+ext_height
                            ext_left = rib_top_py+ext_height+curve_height
                            cr.line_to(px, ext_left)
                            x1=x2 = px+node_depth
                            y1 = ext_left-curve_height/3.
                            y2 = ext_left-2.*curve_height/3.
                            cr.curve_to(x1,y1,x2,y2,px,top_ext_py)
                            cr.line_to(px, rib_top_py)
                            cr.line_to(px+rib_width,rib_top_py)
                    
                            # Straight line
                            #cr.line_to(px+rib_width,rib_end_py)
                    
                            # Bezier curve
                            cr.line_to(px+rib_width,top_ext_py)
                            x1=x2 = px+rib_width-node_depth
                            y2 = top_ext_py+2.*curve_height/3.
                            y1 = top_ext_py+curve_height/3.
                            cr.curve_to(x1,y1,x2,y2,px+rib_width,top_ext_py+curve_height)
                            cr.line_to(px+rib_width,rib_end_py)
                            cr.stroke()
                            cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

                    x += 1.0/resolution

                    ## Determine where to draw top of next rib cutout
                    #rib_top_y = max(line[i:i+rib_width_slices])
                    ##rib_top_y = y+rib_height
                    #if not rib_heights.has_key(rib_idx):
                    #   rib_heights[rib_idx] = []
                    #rib_heights[rib_idx].append(rib_top_y)

            if last_line_page and next_line is not None:
                create_page_row("c",len(crs), pss, crs, pages_wide, page_width, page_height, format)
                if profile:
                    # Draw all of the lines close to the bottom of the page in profile mode
                    y = page_height-50
                else:
                    y = (len(crs)-1)*page_height+max(next_line)
                first_lines.append(line_idx+1)

            if not last_line_page:
                if not profile:
                    y += delta_y+min_slice

            for cr in list(mod_crs.keys()):
                cr.stroke()
            
        print("min_delta_y:", min_delta_y)
        print("min_delta_idx:", min_delta_idx)

        for crr in crs:
            for cr in crr:
                cr.stroke()

        self.rib_heights = rib_heights

    def draw_ribs(self):
        rib_heights = self.rib_heights
        thickness = self.config.thickness
        min_rib = self.config.min_rib
        page_width = self.config.page_width
        page_height = self.config.page_height
        xbar_spacing = self.config.xbar_spacing
        min_xbar = self.config.min_xbar
        xbar_thickness = self.config.xbar_thickness
        xbar_height = self.config.xbar_height
        title = self.config.title
        label_font = self.config.label_font
        title_font = self.config.title_font
        cut_color = self.config.cut_color
        interior_color = self.config.interior_color
        etch_color = self.config.etch_color
        kerf = self.config.kerf
        format = self.config.format

        width = len(list(rib_heights.values())[0])*thickness
        if width % page_width == 0:
            pages_wide = int(width/page_width)
        else:
            pages_wide = int(width/page_width)+1

        pss = []
        crs = []
        y = 0
    
        xbar_widths = defaultdict(lambda: [])

        min_rib_height = min([val for sublist in list(rib_heights.values()) for val in sublist])

        rib_count = 0
        for rib_idx in sorted(rib_heights.keys()):
            # Scale rib heights to minimum (otherwise the ribs can be very tall because their height depends on absolute elevation)
            rib = [h-min_rib_height for h in rib_heights[rib_idx]]
            last_rib = rib_idx == sorted(rib_heights.keys())[-1]
            mod_crs = {}
            x = 0
            y += min_rib+max(rib)

            # Label ribs starting with "A"
            rib_id = chr(ord("A")+rib_count)

            cr_row = int(y/page_height)

            kerf_delta = 0
            for i in range(len(rib)):
                height = rib[i]
            
                # next_height is necessary for calculating kerf for vertical lines
                try:
                    next_height = rib[i+1]
                except IndexError:
                    next_height = height

                try:
                    cr = crs[cr_row][int(x/(page_width))]
                except IndexError:
                    create_page_row("r", len(crs), pss, crs, pages_wide, page_width, page_height, format)
                    try:
                        cr = crs[cr_row][int(x/(page_width))]
                    except Exception as exp:
                        import pdb;pdb.set_trace()
                    y = (len(crs)-1)*page_height+max(rib)+min_rib

                mod_crs[cr] = True

                cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])
                cr.select_font_face(label_font, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
                cr.set_font_size(20)

                px = (x)%page_width
                py = (y-(height+min_rib))%page_height

                if not last_rib and i % 10 == 0 and i > 0 and i < len(rib)-10:
                    # Draw numbered label for ease of assembly (but not on last rib because these labels will be visible in the final product)
                    cur = cr.get_current_point()
                    cr.stroke()
                    cr.set_source_rgb(etch_color[0],etch_color[1],etch_color[2])
                    cr.move_to(px,(y-(xbar_height+xbar_thickness+10))%page_height)
                    cr.show_text(str(i))
                    cr.stroke()
                    cr.move_to(cur[0],cur[1])
                    cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

                # The last rib will be visible in the final product, so draw a title instead of numbered labels
                if last_rib and i == 1:
                    cur = cr.get_current_point()
                    cr.stroke()
                    cr.select_font_face(title_font, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
                    cr.set_source_rgb(etch_color[0],etch_color[1],etch_color[2])
                    cr.move_to(20,(y-20)%page_height)
                    cr.show_text(title)
                    cr.stroke()
                    cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])
                    cr.move_to(cur[0],cur[1])
                    cr.select_font_face(label_font, cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)

                if i == 0:
                    if not last_rib:
                        # Draw rib id label (but not on last rib because these labels will be visible in the final product)
                        cur = cr.get_current_point()
                        cr.stroke()
                        cr.move_to(20,(y-20)%page_height)
                        cr.set_source_rgb(etch_color[0],etch_color[1],etch_color[2])
                        cr.show_text(rib_id)
                        cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])
                        cr.stroke()
                        cr.move_to(cur[0],cur[1])

                    # Draw left side line
                    # Adding small offset to move line away from very edge of page -- may not be necessary
                    px += 0.01
                    x += 0.01
                    cr.move_to(px, (y%page_height))

                cr.line_to(px+kerf_delta,py)
                if next_height > height:
                    kerf_delta = -kerf
                elif next_height < height:
                    kerf_delta = +kerf
                else:
                    kerf_delta = 0
                cr.line_to(px+kerf_delta+thickness,py)

                if i == len(rib)-1 or (px+thickness)%page_width < px:
                    # Draw right side line
                    cr.line_to(px+thickness,(y%page_height))
                    # Draw bottom line
                    cr.line_to(0,(y%page_height))

                if (i+xbar_spacing/2) % xbar_spacing == 0:
                    # FIXME -- any reason to do this inside this loop? Probably easier to calculate in absolute position terms
                    # FIXME -- need to make sure this works for both even and add numbers of ribs:
                    num_ribs = len(list(rib_heights.keys()))
                    max_width = min_xbar+xbar_thickness*num_ribs/2
                    if rib_count < num_ribs/2:
                        xbar_width = min_xbar + xbar_thickness*rib_count
                    else:
                        xbar_width = min_xbar + xbar_thickness*(num_ribs-1-rib_count)
                    xbar_widths[i].append(xbar_width)
                    #abs(-rib_idx+rib_idx%(len(rib_heights.keys())/2))
                    # Draw crossbar cutouts
                    print("%s XBAR %s: %s/%s (%s,%s)" % (rib_id, i, x, px, px+(max_width-xbar_width)/2,(y-xbar_height)%page_height)) 
                    cur = cr.get_current_point()
                    cr.stroke()
                    cr.set_source_rgb(interior_color[0],interior_color[1],interior_color[2])
                    cr.move_to(px-xbar_width/2,(y-xbar_height)%page_height)
                    cr.rel_line_to(0,-xbar_thickness+kerf/2.0)
                    cr.rel_line_to(xbar_width-kerf/2.0,0)
                    cr.rel_line_to(0,xbar_thickness-kerf/2.0)
                    cr.rel_line_to(-xbar_width+kerf/2.0,0)
                    cr.stroke()
                    cr.move_to(cur[0],cur[1])
                    cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

                x += thickness
                # FIXME -- hack so that kerf doesn't cause page overflow -- would have to be fixed to make multipage width work
                if x > page_width:
                    x = page_width-.01
            rib_count += 1

            for cr in list(mod_crs.keys()):
                cr.stroke()
        self.xbar_widths = xbar_widths

    def draw_xbars(self):
        xbar_widths = self.xbar_widths
        page_width = self.config.page_width
        page_height = self.config.page_height
        rib_thickness = self.config.rib_width
        rib_spacing = self.config.rib_spacing
        node_depth = self.config.node_depth
        cut_color = self.config.cut_color
        interior_color = self.config.interior_color
        etch_color = self.config.etch_color
        kerf = self.config.kerf
        format = self.config.format

        print("xbar_widths:", xbar_widths)
        print("rib_thickness:", rib_thickness)
        print("rib_spacing:", rib_spacing)
        print("num_ribs:", len(list(xbar_widths.keys())))
        width = 2*rib_thickness+(len(list(xbar_widths.keys()))-2)*rib_spacing
        print("width:", width)
        if width % page_width == 0:
            pages_wide = int(width/page_width)
        else:
            pages_wide = int(width/page_width)+1

        print("pages_wide:", pages_wide)

        pss = []
        crs = []
        y = 0
        x = 0

        for xbar_idx in sorted(xbar_widths.keys()):
            mod_crs = {}
            xbar = xbar_widths[xbar_idx]

            # Insert extra width in the middle to make the ridges for the middle ribs
            max_xbar = max(xbar)+rib_thickness
            xbar = xbar[0:len(xbar)/2]+[max_xbar]+xbar[len(xbar)/2:]

            y += max(xbar)
            x = 1

            cr_row = int(y/page_height)

            try:
                cr = crs[cr_row][int(x/(page_width))]
            except IndexError:
                create_page_row("x", len(crs), pss, crs, pages_wide, page_width, page_height, format)
                cr = crs[cr_row][int(x/(page_width))]
                y = (len(crs)-1)*page_height+max(xbar)

            mod_crs[cr] = True
            cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

            py = y%page_height

            mid = py-max(xbar)/2.0
            last_width = 0

            for i in range(len(xbar)):
                px = x%page_width
                c = (0,0,0)
                cr.set_source_rgb(c[0],c[1],c[2])
                width = xbar[i]
                if i == 0 or i == len(xbar)-1:
                    height = rib_thickness

                    # Draw left side
                    #cr.move_to(px,mid-width/2)
                    #cr.line_to(px,mid+width/2)
                    #last_width=width
                else:
                    height = rib_spacing

                if height < rib_thickness:
                    node_height = height
                    height = 0
                else:
                    node_height = rib_thickness
                    height = height - rib_thickness

                cr.move_to(px,mid-last_width/2)
                cr.rel_line_to(0, (last_width-width)/2)
                if i < len(xbar)/2:
                    cr.rel_line_to(height, 0)
                    cr.rel_curve_to(node_height/3., -node_depth, 2*node_height/3., -node_depth, node_height,0)
                elif i == len(xbar)/2:
                    cr.rel_line_to(height+node_height, 0)
                else:
                    cr.rel_curve_to(node_height/3., -node_depth, 2*node_height/3., -node_depth, node_height,0)
                    cr.rel_line_to(height, 0)
                #cr.rel_line_to(height, 0)
                cr.move_to(px,mid+last_width/2)
                cr.rel_line_to(0, -(last_width-width)/2)
                if i < len(xbar)/2:
                    cr.rel_line_to(height, 0)
                    cr.rel_curve_to(node_height/3., node_depth, 2*node_height/3., node_depth, node_height,0)
                elif i == len(xbar)/2:
                    cr.rel_line_to(height+node_height, 0)
                else:
                    cr.rel_curve_to(node_height/3., node_depth, 2*node_height/3., node_depth, node_height,0)
                    cr.rel_line_to(height, 0)
                #cr.rel_move_to(height,0)

                if i == len(xbar)-1:
                    # Draw right side
                    cr.rel_line_to(0,-width)

                x += height+node_height

                last_width = width
                cr.stroke()

            # Draw mounting holes
            cr.stroke()
            cr.set_source_rgb(interior_color[0],interior_color[1],interior_color[2])
            cr.arc(x/3.0, (y-max_xbar/2.0)%page_height, rib_thickness, 0, 2*pi)
            cr.stroke()
            cr.arc(2.0*x/3.0, (y-max_xbar/2.0)%page_height, rib_thickness, 0, 2*pi)
            cr.stroke()
            cr.set_source_rgb(cut_color[0],cut_color[1],cut_color[2])

            for cr in list(mod_crs.keys()):
                cr.stroke()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("config", help="Config file")
    parser.add_argument("-s", "--state", help="State file", type=str, default=None)
    args = parser.parse_args()

    if not os.path.isfile(args.config):
        print("ERROR: config file '%s' not found" % (args.config))
        sys.exit(1)

    config = SampleConfig()
    exec(compile(open(args.config).read(), args.config, 'exec'), {'top': config})

    print("Configuration:")
    print(config)

    if args.state is not None and os.path.isfile(args.state):
        d = pickle.load(open(args.state))
        d.update_config(config)
    else:
        d = Drawer(config, state=args.state)
    d.set_state(args.state)
    d.draw()    

if __name__ == '__main__':
    main()
