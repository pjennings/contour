#!/usr/bin/env python

from Config import Config


def color(c):
    if type(c) == str:
        try:
            c = [int(cp) for cp in c.split(",")]
        except:
            raise ValueError("'%s' is not a valid color -- use r,g,b format, where r,g,b are integers 0-255 (bad format)" % c)
    if len(c) != 3:
        raise ValueError("'%s' is not a valid color -- use r,g,b format, where r,g,b are integers 0-255 (incorrect number of args)" % c)
    for component in c:
        if component < 0 or component > 255:
            raise ValueError("'%d' is not in range 0-255" % component)
    return c

def check_num_ribs(c):
    if c % 2 != 0:
        raise ValueError("num_ribs must be an even number")
    return c

def ppi(v):
    PPI = 72 # FIXME -- put this somewhere else
    return float(v)*PPI

def out_format(v):
    if v.lower() not in ("pdf", "svg"):
        raise ValueError("Unknown output format '%s'" % v)
    return v.lower()

class SampleConfig(Config):
    def __init__(self):
        Config.__init__(self)

        self.add_argument("-r", "--rows",                                         help="Row of tile files, separated by commas")
        self.add_argument("-pw", "--page-width",      default=8,      type=ppi,   help="Output page width in inches")
        self.add_argument("-ph", "--page-height",     default=11,     type=ppi,   help="Output page height in inches")
        self.add_argument("-ow", "--output-width",    default=3*12,   type=ppi,   help="Width of final design in inches")
        self.add_argument("-oh", "--output-height",   default=5*12,   type=ppi,   help="Height of final design in inches")
        self.add_argument("-owx", "--output-resolution", default=10,  type=int,   help="How many 'slices' to create in x direction")
        self.add_argument("-res", "--resolution",     default=0.333,  type=float, help="Resolution of input data in arc-seconds")
        self.add_argument("-lat", "--latitude",       default=45,     type=float, help="Approximate latitude of data, used with res to scale elevation")
        self.add_argument("-t", "--thickness",        default=0.25,   type=ppi,   help="Thickness of cut material")
        self.add_argument("-ms", "--min-slice",       default=0.25,   type=ppi,   help="Minimum height of one slice")
        self.add_argument("-mr", "--min-rib",         default=1.0,    type=ppi,   help="Minimum height of one rib")
        self.add_argument("-nd", "--node-depth",      default=0.01,   type=ppi,   help="Depth of nodes in notches")
        self.add_argument("-sy", "--scale-y",         default=1.0,    type=float, help="Proportion to exaggerate Y scales")
        self.add_argument("-rw", "--rib-width",       default=0.25,   type=ppi,   help="Rib width in inches (thickness of rib material)")
        self.add_argument("-rh", "--rib-height",      default=0.5,    type=ppi,   help="Rib height (how deep vertically into a slice to cut ribs)")
        self.add_argument("-rpw", "--rib-page-width", default=8,      type=ppi,   help="Width of rib material in inches")
        self.add_argument("-rph", "--rib-page-height",default=11,     type=ppi,   help="Height of rib material in inches")
        self.add_argument("-nr", "--num-ribs",        default=6,      type=check_num_ribs,   help="Number of ribs")
        self.add_argument("-nx", "--num-xbars",       default=3,      type=int,   help="Number of crossbars")
        self.add_argument("-mx", "--min-xbar",        default=1.0,    type=ppi,   help="Minimum crossbar width")
        self.add_argument("-tx", "--xbar-thickness",  default=0.25,   type=ppi,   help="Crossbar material thickness")
        self.add_argument("-tt", "--title",           default="",     type=str,   help="Title (text to be drawn on side of last rib)")
        self.add_argument("-xh", "--xbar-height",     default=0.25,   type=ppi,   help="Height of crossbar above bottom of rib")
        self.add_argument("-lf", "--label-font",      default="cairo:monospace",type=str,help="Font to use for drawing labels")
        self.add_argument("-tf", "--title-font",      default="serif",type=str,   help="Font to use for drawing title")
        self.add_argument("-cc", "--cut-color",       default="255,0,0",type=color, help="Color to use for exterior cut lines (r,g,b)")
        self.add_argument("-ci", "--interior-color",  default="0,255,0",type=color,help="Color to use for interior cut lines (r,g,b)")
        self.add_argument("-ce", "--etch-color",      default="0,0,0",type=color,   help="Color to use for etch lines (r,g,b)")
        self.add_argument("-k", "--kerf",             default=0.008,  type=ppi,   help="Laser kerf")
        self.add_argument("-d", "--debug",            default=False,  type=bool,  help="Debug")
        self.add_argument("-p", "--profile",          default=False,  type=bool,  help="Profile mode -- draw all countours on top of each other to show total height of final design")
        self.add_argument("-sf", "--window-function", default='hanning', type=str, help="Window function (hanning, hamming, blackman, bartlett, none)")
        self.add_argument("-sw", "--window-width",    default=11,     type=int,   help="Width of smoothing window (number of samples)")
        self.add_argument("-f", "--format",           default="svg",  type=out_format,   help="Output format (pdf or svg)")
        self.add_argument("-rot", "--rotate",         default=False,  type=bool,  help="Rotate data 90 degrees")
        self.add_argument("-flr", "--flip-lr",        default=True,   type=bool,  help="Flip data left-to-right")
        self.add_argument("-fud", "--flip-ud",        default=True,   type=bool,  help="Flip data up-to-down")
        self.add_argument("-fr", "--force-reload",    default=False,  type=bool,  help="Force reload of data even if stored data is detected")
