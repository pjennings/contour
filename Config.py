#!/usr/bin/env python

class Config(object):
    def __init__(self, parent=None, category="root"):
        self._parent = parent
        self._category = category
        self._subs = {}
        self._config = {}
        self._help = {}
        self._types = {}
        self._short = {}

    def __getstate__(self):
        return {
            'parent': self._parent,
            'category': self._category,
            'subs': self._subs,
            'config': self._config,
            'help': self._help,
            'types': self._types,
            'short': self._short
        }

    def __setstate__(self, state):
        self._parent = state['parent']
        self._category = state['category']
        self._subs = state['subs']
        self._config = state['config']
        self._help = state['help']
        self._types = state['types']
        self._short = state['short']

    def __getitem__(self, key):
        if key in self._subs:
            return self._subs[key]
        self._subs[key] = Config(self, key)
        return self._subs[key]

    def __setitem__(self, key, val):
        raise ImplementationError("Do not set child configs -- just use them")

    def __getattr__(self, attr):
        if attr in self._config:
            return self._config[attr]
        if self._parent is not None:
            return Config.__getattr__(self._parent, attr)
        raise AttributeError

    def __setattr__(self, attr, val, type=None):
        if attr.startswith("_"):
            super(Config, self).__setattr__(attr, val)
        else:
            if attr not in self._config and attr in self._short:
                attr = self._short[attr]
            if type is not None:
                self._types[attr] = type
            if attr in self._types:
                val = self._types[attr](val)
            self._config[attr] = val

    def add_argument(self, short, long, default=None, type=None, help=None):
        key = short.replace("-", "")
        if long is not None:
            key = long.replace("-","_").replace("__", "")
        self.__setattr__(key, default, type=type)
        self._short[key] = short
        self._help[key] = help

    def help(self, c):
        if c in self._help and self._help[c] is not None:
            return self._help[c]
        return "No help available for '%s'" % c

    def keys(self):
        r = set(self._config.keys())
        if self._parent is not None:
            r = r.union(set(self._parent.keys()))
        return list(r)

    def __repr__(self):
        r = self._category+" config:\n"
        for key in list(self.keys()):
            if key in list(self._config.keys()):
                if self._parent is not None and key in list(self._parent.keys()):
                    mine = ">"
                else:
                    mine = "*"
            else:
                mine = "^"
            r += "%20s%1s: %s\n" % (key, mine, self.__getattr__(key))
        return r

